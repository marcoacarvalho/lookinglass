from django.contrib import admin

# Register your models here.
from .models import Router, Routertype

admin.site.register(Router)
admin.site.register(Routertype)
