from django.views.decorators.csrf import csrf_protect
from django.shortcuts import render
import paramiko
import re

# Create your views here.
from .models import Router, Routertype


@csrf_protect
def index(request):
    routers = Router.objects.all()
    if request.method == "POST":
        data = []
        data_v6 = []
        command_choice = request.POST.get('command_choice')
        if int(command_choice) > 1:
            ip = request.POST.get('ip')
            if ip:
                if chekIP(ip):
                    pass
                else:
                    ip = ''
        router_id = request.POST.get('router_id')
        router = Router.objects.get(id=router_id)
        router_name = router.router_name
        router_ip = router.router_ip
        router_user = router.router_user
        router_password = router.router_password
        router_type = router.router_type.id
        router_commands = Routertype.objects.get(id=router_type)
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        try:
            ssh.connect(router_ip, username=router_user, password=router_password)
        except:
            command_choice = '0'
        # print(router_commands.router_type)
        if command_choice == "1":
            command = router_commands.sh_summ
            stdin, stdout, stderr = ssh.exec_command(command)
            lines = stdout.read().decode('utf8').splitlines()
            tot_peers = lines[0][18:20]
            down_peers = lines[0][33:]
            tot_routes = int(lines[3][:24])
            tot_routes_installed = int(lines[3][25:36])
            tot_routes_v6 = int(lines[5][:24])
            tot_routes_installed_v6 = int(lines[5][25:36])
            up_peers = int(tot_peers) - int(down_peers)
            # print(lines[6])
            # print(lines[7])
            # print(lines[8])
            for line in lines[7:]:
                peer = line[:15]
                asnum = line[15:27]
                timeud = line[65:77]
                state = line[78:].split(' ')[0].replace('/', ' | ')
                if 'inet' in peer:
                    continue
                if ':' in peer:
                    continue
                if 'Establ' in state:
                    state = lines[lines.index(line) + 1][9:].split(' ')[1].replace('/', ' | ')
                # print(peer, asnum, timeud, state)
                data_line = [peer, asnum, timeud, state]
                data.append(data_line)
            for line in lines[7:]:
                if 'inet' in line.split(' ', 7)[0]:
                    continue
                if ':' in line.split(' ', 7)[0]:
                    linelist = line.split()
                    # print(linelist)
                    peer = linelist[0]
                    asnum = linelist[1]
                    if len(linelist) == 9:
                        timeud = linelist[6] + ' ' + linelist[7]
                        state = linelist[8]
                    else:
                        timeud = linelist[6]
                        state = linelist[7]
                    if 'Establ' in state:
                        state = lines[lines.index(line) + 1].split()[1].replace('/', ' | ')
                    # print(peer, asnum, timeud, state)
                    data_line_v6 = [peer, asnum, timeud, state]
                    data_v6.append(data_line_v6)
            context = {'routers': routers,
                       'data': data,
                       'data_v6': data_v6,
                       'command_choice': command_choice,
                       'tot_peers': tot_peers,
                       'up_peers': up_peers,
                       'tot_routes': tot_routes,
                       'tot_routes_installed': tot_routes_installed,
                       'tot_routes_v6': tot_routes_v6,
                       'tot_routes_installed_v6': tot_routes_installed_v6,
                       'router_name': router_name}
        elif command_choice == "2":
            if ip:
                command = router_commands.sh_route + ' ' + ip
                stdin, stdout, stderr = ssh.exec_command(command)
                data = stdout.read().decode('utf8')
                regex = r'via.*'
                data = re.sub(regex, '', data)
                regex = '\[.*\]'
                if 'BGP' in data:
                    data = re.sub(regex, '[eBGP]', data)
                elif 'OSPF' in data:
                    data = re.sub(regex, '[IGP]', data)
                data = data.splitlines()
                print(data)
            else:
                command = 'quit'
            context = {'routers': routers,
                       'data': data,
                       'command_choice': command_choice,
                       'ip': ip}

        elif command_choice == "3":
            if ip:
                command = router_commands.ping + ' ' + ip
                stdin, stdout, stderr = ssh.exec_command(command)
                data = stdout.read().decode('utf8').splitlines()
            else:
                command = 'quit'
            context = {'routers': routers,
                       'data': data,
                       'command_choice': command_choice,
                       'ip': ip}

        elif command_choice == "4":
            if ip:
                command = router_commands.traceroute4 + ' ' + ip
                stdin, stdout, stderr = ssh.exec_command(command)
                data = stdout.read().decode('utf8').splitlines()
            else:
                command = 'quit'
            context = {'routers': routers,
                       'data': data,
                       'command_choice': command_choice,
                       'ip': ip}
        elif command_choice == "5":
            if ip:
                command = router_commands.traceroute6 + ' ' + ip
                data = []
                stdin, stdout, stderr = ssh.exec_command(command)
                data = stdout.read().decode('utf8').splitlines()
            else:
                command = 'quit'
            context = {'routers': routers,
                       'data': data,
                       'command_choice': command_choice,
                       'ip': ip}
        elif command_choice == "0":
            data = ['Query rate exceeded, please try again later']
            context = {'routers': routers,
                       'data': data}

        # print(command)
        return render(request, 'index.html', context)
    else:
        context = {'routers': routers}
        return render(request, 'index.html', context)
"""    for line in data[:8]:
        # print(line)
    # print('===============================================')
    for line in data[9:]:
        # print(line.split())
        # print('---------------------------------')
"""


def chekIP(ip):
    ipv4 = re.compile(
        '^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$')
    ipv6 = re.compile('^s*((([0-9A-Fa-f]{1,4}:){7}([0-9A-Fa-f]{1,4}|:))|(([0-9A-Fa-f]{1,4}:){6}(:[0-9A-Fa-f]{1,4}|((25[0-5]|2[0-4]d|1dd|[1-9]?d)(.(25[0-5]|2[0-4]d|1dd|[1-9]?d)){3})|:))|(([0-9A-Fa-f]{1,4}:){5}(((:[0-9A-Fa-f]{1,4}){1,2})|:((25[0-5]|2[0-4]d|1dd|[1-9]?d)(.(25[0-5]|2[0-4]d|1dd|[1-9]?d)){3})|:))|(([0-9A-Fa-f]{1,4}:){4}(((:[0-9A-Fa-f]{1,4}){1,3})|((:[0-9A-Fa-f]{1,4})?:((25[0-5]|2[0-4]d|1dd|[1-9]?d)(.(25[0-5]|2[0-4]d|1dd|[1-9]?d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){3}(((:[0-9A-Fa-f]{1,4}){1,4})|((:[0-9A-Fa-f]{1,4}){0,2}:((25[0-5]|2[0-4]d|1dd|[1-9]?d)(.(25[0-5]|2[0-4]d|1dd|[1-9]?d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){2}(((:[0-9A-Fa-f]{1,4}){1,5})|((:[0-9A-Fa-f]{1,4}){0,3}:((25[0-5]|2[0-4]d|1dd|[1-9]?d)(.(25[0-5]|2[0-4]d|1dd|[1-9]?d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){1}(((:[0-9A-Fa-f]{1,4}){1,6})|((:[0-9A-Fa-f]{1,4}){0,4}:((25[0-5]|2[0-4]d|1dd|[1-9]?d)(.(25[0-5]|2[0-4]d|1dd|[1-9]?d)){3}))|:))|(:(((:[0-9A-Fa-f]{1,4}){1,7})|((:[0-9A-Fa-f]{1,4}){0,5}:((25[0-5]|2[0-4]d|1dd|[1-9]?d)(.(25[0-5]|2[0-4]d|1dd|[1-9]?d)){3}))|:)))(%.+)?s*')
    cidr = re.compile(
        '^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])(\/([0-9]|[1-2][0-9]|3[0-2]))$')

    isipv4 = ipv4.match(ip)
    isipv6 = ipv6.match(ip)
    iscidr = cidr.match(ip)
    ishostname = is_valid_hostname(ip)
    # print(isipv4, isipv6, iscidr, ishostname)
    if isipv4 or isipv6 or iscidr or ishostname:
        return True
    else:
        return False


def is_valid_hostname(hostname):
    if len(hostname) > 255:
        return False
    if hostname[-1] == '.':
        hostname = hostname[:1]
    allowed = re.compile("(?!-)[A-Z\d-]{1,63}(?<!-)$", re.IGNORECASE)
    return all(allowed.match(x) for x in hostname.split('.'))
