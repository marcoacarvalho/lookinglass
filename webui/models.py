from django.db import models


# Create your models here.
class Router(models.Model):

    router_name = models.CharField(max_length=200)
    router_ip = models.CharField(max_length=20)
    router_user = models.CharField(max_length=20)
    router_password = models.CharField(max_length=20)
    router_type = models.ForeignKey('Routertype')


class Routertype(models.Model):
    router_type = models.CharField(max_length=20)
    sh_summ = models.CharField(max_length=200)
    sh_route = models.CharField(max_length=200)
    ping = models.CharField(max_length=200)
    traceroute4 = models.CharField(max_length=200)
    traceroute6 = models.CharField(max_length=200)
